# Karma Perfecto Integration Project

This project is designed to show the Perfecto / Karma integration for testing Web Applications during design phase using [Karma](https://karma-runner.github.io/3.0/index.html).  The initial solution is being created for Vanguard.

The Perfecto Solution is being worked on by: Morris Horesh

This Project Maintained by: Dave Denver


## Setup

* Install npm
* Run ``` npm install ```
    * This will install all of the npm packages listed in the package.json file
* Run ``` npm install -g karma-cli```
    * This step is optional.  If you skip it, you must modify the package.json file from
    ```
      "scripts": {
        "test": "karma start karma.conf.js"
      },
    ```
    to

    ```  
    "scripts": {
           "test": "./node_modules/karma/bin/karma start karma.conf.js"
         },
     ```


## Running Tests
* To run tests
```angular2
npm test
```


## Project Structure
```angular2
.
├── README.md
├── calculator.html
├── karma.conf.js
├── lib
│   └── calculator.js
├── package.json
└── test
    └── calculator.test.js

```
* [calculator.html](./calculator.html) - Web Page file to add 2 numbers
* [karma.conf.js](./karma.conf.js) - Karma configuration file - contains browser, specs, Perfecto settings (upcoming), etc..
* [lib/calculator.js](./lib/calculator.js) - Contains .js code for making the simple web page work
* [package.json](./package.json) - Contains the npm setup used when running ```npm install```
* [test/calculator.test.js](./test/calculator.test.js) - Contains tests run by Karma
