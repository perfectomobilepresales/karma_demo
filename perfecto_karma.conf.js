module.exports = function(config) {


const customLaunchers = {
  chrome72: {
    base: 'Perfecto',
    capabilities: {
	  platformName : 'Windows',
      platformVersion : '10',
      browserName : 'Chrome',
      browserVersion : '72',
      resolution : '1280x1024',
      location : 'US East'
	}
  },

  chrome71: {
    base: 'Perfecto',
    capabilities: {
	  platformName : 'Windows',
      platformVersion : '10',
      browserName : 'Chrome',
      browserVersion : '71',
      resolution : '1280x1024',
      location : 'US East'
	}
  },
};

  config.set({

    // It may take some time to initialize a connection
    captureTimeout: 120000,

    perfecto: {
		// Set if there is already a running tunnel.
		// The environment variable PERFECTO_TUNNEL_ID can be set instead.
		// If this parameter is not set, the launcher will start a new tunnel
		tunnelId: '',

		// The local hostname or host address.
		// If not set this value is determined automatically
		    host: '',

		// The full path of perfectoconnect binary.
		// If not set it is assumed that the binary is in the executable path
		perfectoConnect: '',

		// The Perfecto Cloud URL (required)
		perfectoUrl: 'https://demo.perfectomobile.com/nexperience/perfectomobile/wd/hub/fast',

		// The security token (required)
		securityToken: '',

		// the name of the test for the report
		testName: ''

    },


    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'lib/*.js',
        'test/*.js'
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    //browsers: ['Perfecto'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    customLaunchers: customLaunchers,
    browsers: Object.keys(customLaunchers),

    reporters: ['progress']
  })
}
