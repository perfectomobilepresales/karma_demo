// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  const customLaunchers = {
    chrome72: {
      base: 'Perfecto',
      capabilities: {
  	  platformName : 'Windows',
        platformVersion : '10',
        browserName : 'Chrome',
        browserVersion : '72',
        resolution : '1280x1024',
        location : 'US East'
  	}
    },

    chrome71: {
      base: 'Perfecto',
      capabilities: {
  	  platformName : 'Windows',
        platformVersion : '10',
        browserName : 'Chrome',
        browserVersion : '71',
        resolution : '1280x1024',
        location : 'US East'
  	}
    },
  };

  config.set({
    // It may take some time to initialize a connection
    captureTimeout: 120000,

    /// Perfecto Specific setup parameters
    perfecto: {
		// Set if there is already a running tunnel.
		// The environment variable PERFECTO_TUNNEL_ID can be set instead.
		// If this parameter is not set, the launcher will start a new tunnel
		tunnelId: '',

		// The local hostname or host address.
		// If not set this value is determined automatically
		host: '',

		// The full path of perfectoconnect binary.
		// If not set it is assumed that the binary is in the executable path
		perfectoConnect: '',

		// The Perfecto Cloud URL (required)
		perfectoUrl: 'https://demo.perfectomobile.com/nexperience/perfectomobile/wd/hub/fast',

		// The security token (required)
		securityToken: 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJzbFV4OFFBdjdVellIajd4YWstR0tTbE43UjFNSllDbC1TRVJiTlU1RFlFIn0.eyJqdGkiOiI1MzA3OTYxMi02NDRkLTQzYWMtOWRjYS0yMDE0N2EwMmRkZmEiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTM4NDA5Mjk0LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2RlbW8tcGVyZmVjdG9tb2JpbGUtY29tIiwiYXVkIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJzdWIiOiI0YzUxZTRkYS1mMDdhLTQ0ZDEtOWMxOS02N2VjZDQwNWJmMjciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6IjI0YTQ4N2ZjLWNjNmMtNDRmMi04NmQyLTk1MzkzMjBiZjNkMSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6Ijc0OWVlYjczLTdlYjItNDdmZS04NTE2LWNjNjEyYmE4ZWFkZiIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fX0.MUMYiKvdWQVWt0Cel-vNLB_xeToKijlH5hpC4BpPTI561qDUpMfB2nluYUBVt5PyUCaSOR4q2cVcLPsG3TofDTpoOAD0OkM3Y5LnZb82mU6eCH9UYcYBs5Rts0GhNWkZV8PZfEAwnBGIs-e_yiUnW8V-KYXOVUU0mTnfEFCjjQZvYUbZJbIuUHN9-rj96k31LfXYLrHbMNQ5Rj7JgCXJ6RCqFYOMGhvf24nTGfgOxdDRIqAgWM7WUWz_E2jdEeAMa8xZPGQuAeozi3glGAbIYA6k-zCA_t1perzne_R-zJzpnpUOsnh5DN54iKyqlxj2AB83jmvLcvNcHQhn9Zk5bA',

		// the name of the test for the report
		testName: 'Karma_test'

    },


    // Basic Karma settings
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    files: [
        'lib/*.js',
        'test/*.js'
    ],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage/karma'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: true,
    concurrency: Infinity,
    // restartOnFileChange: false,
    // Tell Karma to use the Perfecto settings above
    customLaunchers: customLaunchers,
    browsers: Object.keys(customLaunchers)
  });
};
