// Karma configuration
// Generated on Tue Mar 12 2019 10:50:21 GMT-0400 (Eastern Daylight Time)

module.exports = function(config) {
  const customLaunchers = {
    chrome72: {
      base: 'Perfecto',
      capabilities: {
  	  platformName : 'Windows',
        platformVersion : '10',
        browserName : 'Chrome',
        browserVersion : '72',
        resolution : '1280x1024',
        location : 'US East'
  	}
    },

    chrome71: {
      base: 'Perfecto',
      capabilities: {
  	  platformName : 'Windows',
        platformVersion : '10',
        browserName : 'Chrome',
        browserVersion : '71',
        resolution : '1280x1024',
        location : 'US East'
  	}
    },
  };

  config.set({
    // It may take some time to initialize a connection
    captureTimeout: 120000,

    /// Perfecto Specific setup parameters
    perfecto: {
		// Set if there is already a running tunnel.
		// The environment variable PERFECTO_TUNNEL_ID can be set instead.
		// If this parameter is not set, the launcher will start a new tunnel
		tunnelId: '',

		// The local hostname or host address.
		// If not set this value is determined automatically
		host: '',

		// The full path of perfectoconnect binary.
		// If not set it is assumed that the binary is in the executable path
		perfectoConnect: '',

		// The Perfecto Cloud URL (required)
		perfectoUrl: 'https://demo.perfectomobile.com/nexperience/perfectomobile/wd/hub/fast',

		// The security token (required)
		securityToken: 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJzbFV4OFFBdjdVellIajd4YWstR0tTbE43UjFNSllDbC1TRVJiTlU1RFlFIn0.eyJqdGkiOiI1MzA3OTYxMi02NDRkLTQzYWMtOWRjYS0yMDE0N2EwMmRkZmEiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTM4NDA5Mjk0LCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2RlbW8tcGVyZmVjdG9tb2JpbGUtY29tIiwiYXVkIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJzdWIiOiI0YzUxZTRkYS1mMDdhLTQ0ZDEtOWMxOS02N2VjZDQwNWJmMjciLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6IjI0YTQ4N2ZjLWNjNmMtNDRmMi04NmQyLTk1MzkzMjBiZjNkMSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6Ijc0OWVlYjczLTdlYjItNDdmZS04NTE2LWNjNjEyYmE4ZWFkZiIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fX0.MUMYiKvdWQVWt0Cel-vNLB_xeToKijlH5hpC4BpPTI561qDUpMfB2nluYUBVt5PyUCaSOR4q2cVcLPsG3TofDTpoOAD0OkM3Y5LnZb82mU6eCH9UYcYBs5Rts0GhNWkZV8PZfEAwnBGIs-e_yiUnW8V-KYXOVUU0mTnfEFCjjQZvYUbZJbIuUHN9-rj96k31LfXYLrHbMNQ5Rj7JgCXJ6RCqFYOMGhvf24nTGfgOxdDRIqAgWM7WUWz_E2jdEeAMa8xZPGQuAeozi3glGAbIYA6k-zCA_t1perzne_R-zJzpnpUOsnh5DN54iKyqlxj2AB83jmvLcvNcHQhn9Zk5bA',

		// the name of the test for the report
		testName: 'Karma_test'

    },

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'lib/*.js',
      'test/*.js'
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before sferving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      // 'lib/*.js': 'coverage'
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress','coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    // browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    // Tell Karma to use the Perfecto settings above
    customLaunchers: customLaunchers,
    browsers: Object.keys(customLaunchers)

  })
}
